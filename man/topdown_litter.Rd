\name{topdown_litter}
\alias{topdown_litter}
\title{topdown_litter}
\usage{
topdown_litter(I_vec,litter_vec,mu_l)
}
\description{
This function computes the fraction of producer biomass production that is consumed by consumers.
}
\arguments{
\item{I_vec}{Biomass production rates in each cell.}
\item{litter_vec}{Vector of producer biomass.}
\item{mu_l}{Leaching rate of producer biomass.}
}
\value{
A vector of the fraction of producer biomass production that is consumed by consumers in each cell.
}
\examples{
res=simul_spatial_food_chain_gaussian_edge_litter(0.5,0.9,1,array(0.5,100),array(0.01,100),100,1,1,1,1,1,0.1,0.5,0.01,0.01,0.05,0.05,0.1,0.1,0.1,0.5,0.5,nstep_element=2,n_step=5,kernel_precision=0.99999)
topdown_litter(c(array(0.95,50),array(0.05,50)),res[[5]][[1]],0.1)
}
