#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
int reflect(int j, int ncell){
 if (j<0){
  return -(j+1);
 }
 if (j>=ncell){
    return (2*ncell-1-j);
 }
 return j;
}


// [[Rcpp::export]]
List spatial_foodchain_dynamics_reflective_boundary(NumericVector litter_vec_ini,NumericVector consumer_vec_ini,NumericVector predator_vec_ini, int ncell,double deltaX,double sigma_Fc,double sigma_Fp,double sigma_Dc,double sigma_Dp,NumericVector I_litter,double epsilon_c,double epsilon_p,double d_c,double d_p,double mu_c,double mu_p,double mu_l, double a_c, double a_p, double h_c, double h_p, int nstep, double kernel_precision){
 NumericVector new_litter_vec(ncell);
 NumericVector new_consumer_vec(ncell);
 NumericVector new_predator_vec(ncell);

 NumericVector litter_vec=clone(litter_vec_ini);
 NumericVector consumer_vec=clone(consumer_vec_ini);
 NumericVector predator_vec=clone(predator_vec_ini);

 NumericVector Fc(ncell);
 NumericVector Fp(ncell);
 NumericVector Dc(ncell);
 NumericVector Dp(ncell);
 double denominator_Fc=0.0;
 double denominator_Fp=0.0;
 double denominator_Dc=0.0;
 double denominator_Dp=0.0;
 Fc.fill(0);
 Fp.fill(0);
 Dc.fill(0);
 Dp.fill(0);
 for (int i=0;i<ncell;i++){
  Fc[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Fc*sigma_Fc)));
  Fp[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Fp*sigma_Fp)));
  Dc[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Dc*sigma_Dc)));
  Dp[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Dp*sigma_Dp)));
 }
 for (int j=0;j<(2*ncell-1);j++){
  denominator_Fc=denominator_Fc+Fc[(abs(ncell-1-j))];
  denominator_Fp=denominator_Fp+Fp[(abs(ncell-1-j))];
  denominator_Dc=denominator_Dc+Dc[(abs(ncell-1-j))];
  denominator_Dp=denominator_Dp+Dp[(abs(ncell-1-j))];
 }
 for (int i=0;i<ncell;i++){
  Fc[i]=Fc[i]/denominator_Fc;
  Fp[i]=Fp[i]/denominator_Fp;
  Dc[i]=Dc[i]/denominator_Dc;
  Dp[i]=Dp[i]/denominator_Dp;
 }

 int n_kernel_fc=ncell-1;
 int n_kernel_fp=ncell-1;
 int n_kernel_dc=ncell-1;
 int n_kernel_dp=ncell-1;
 if (kernel_precision<1.0){
  int it=1;
  double test=Fc[0];
  while(it<ncell){
   test+=(2*Fc[it]);
   if (test>kernel_precision){
    n_kernel_fc=it;
    it=ncell;
   }
   it++;
  }
  it=1;
  test=Fp[0];
  while(it<ncell){
   test+=(2*Fp[it]);
   if (test>kernel_precision){
    n_kernel_fp=it;
    it=ncell;
   }
   it++;
  }
  it=1;
  test=Dc[0];
  while(it<ncell){
   test+=(2*Dc[it]);
   if (test>kernel_precision){
    n_kernel_dc=it;
    it=ncell;
   }
   it++;
  }
  it=1;
  test=Dp[0];
  while(it<ncell){
   test+=(2*Dp[it]);
   if (test>kernel_precision){
    n_kernel_dp=it;
    it=ncell;
   }
   it++;
  }
 }

 double integral_lc;
 double integral_cl;
 double integral_cp;
 double integral_cdc;
 double integral_pc;
 double integral_pdp;

 for (int istep=0;istep<nstep;istep++){
  for (int i=0;i<ncell;i++){
   new_litter_vec[i]=litter_vec[i]*(1.0-mu_l);
   new_consumer_vec[i]=consumer_vec[i]*(1.0-mu_c*consumer_vec[i]);
   new_predator_vec[i]=predator_vec[i]*(1.0-mu_p*predator_vec[i]);
  }
  for (int i=0;i<ncell;i++){
   integral_lc=0.0;
   integral_cl=0.0;
   for (int j=(i-n_kernel_fc);j<(i+n_kernel_fc+1);j++){
    integral_lc-=(Fc[(abs(j-i))]*new_consumer_vec[(reflect(j,ncell))]);
    integral_cl+=(Fc[(abs(j-i))]*new_litter_vec[(reflect(j,ncell))]/(1.0+h_c*a_c*new_litter_vec[(reflect(j,ncell))]));
   }

   integral_cp=0.0;
   integral_pc=0.0;
   for (int j=(i-n_kernel_fp);j<(i+n_kernel_fp+1);j++){
    integral_cp-=(new_predator_vec[(reflect(j,ncell))]*Fp[(abs(j-i))]);
    integral_pc+=(Fp[(abs(j-i))]*new_consumer_vec[(reflect(j,ncell))]/(1.0+h_p*a_p*new_consumer_vec[(reflect(j,ncell))]*(1.0-d_c)));
   }

   integral_cdc=0.0;
   for (int j=(i-n_kernel_dc);j<(i+n_kernel_dc+1);j++){
    integral_cdc+=(Dc[(abs(j-i))]*new_consumer_vec[(reflect(j,ncell))]);
   }

   integral_pdp=0.0;
   for (int j=(i-n_kernel_dp);j<(i+n_kernel_dp+1);j++){
    integral_pdp+=(Dp[(abs(j-i))]*new_predator_vec[(reflect(j,ncell))]);
   }

   litter_vec[i]=new_litter_vec[i]+I_litter[i]+((1.0-d_c)*a_c*new_litter_vec[i]*integral_lc/(1.0+h_c*a_c*new_litter_vec[i]));
   consumer_vec[i]=((1.0-d_c)*new_consumer_vec[i])+(epsilon_c*(1.0-d_c)*new_consumer_vec[i]*a_c*integral_cl)+((1.0-d_p)*integral_cp*a_p*(1.0-d_c)*new_consumer_vec[i]/(1.0+h_p*a_p*(1.0-d_c)*new_consumer_vec[i]))+(d_c*integral_cdc);
   predator_vec[i]=((1.0-d_p)*new_predator_vec[i])+(epsilon_p*(1.0-d_p)*new_predator_vec[i]*a_p*(1.0-d_c)*integral_pc)+(d_p*integral_pdp);
  }
 }

 List res = List::create(litter_vec,consumer_vec,predator_vec);
 return res;
}


// [[Rcpp::export]]
NumericVector compute_gaussian_litter(double I_mean,double I_diff,double sigma_I,int ncell,double deltaX){
 NumericVector I_litter(ncell);
 NumericVector I(ncell);
 I_litter.fill(0);
 I.fill(0);

 double denominator_I=0.0;
 for (int i=0;i<ncell;i++){
  I[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_I*sigma_I)));
 }
 for (int j=0;j<(2*ncell-1);j++){
  denominator_I=denominator_I+I[(abs(ncell-1-j))];
 }
 for (int i=0;i<ncell;i++){
  I[i]=I[i]/denominator_I;
 }

 double I_left=I_mean+0.5*I_diff;
 double I_right=I_mean-0.5*I_diff;

 for (int i=0;i<0.5*ncell;i++){
    for (int j=(-ncell+1);j<ncell;j++){
     I_litter[reflect((i+j),ncell)]=I_litter[reflect((i+j),ncell)]+I_left*I[abs(j)];
    }
 }
 for (int i=0.5*ncell;i<ncell;i++){
    for (int j=(-ncell+1);j<ncell;j++){
     I_litter[reflect((i+j),ncell)]=I_litter[reflect((i+j),ncell)]+I_right*I[abs(j)];
    }
 }

 return I_litter;
}


// [[Rcpp::export]]
List compute_bottom_up_tritrophic_cpp(NumericVector litter_vec,NumericVector consumer_vec,NumericVector predator_vec, int ncell,double deltaX,double sigma_Fc,double sigma_Fp,double sigma_Dc,double sigma_Dp,NumericVector I_litter,double epsilon_c,double epsilon_p,double d_c,double d_p,double mu_c,double mu_p,double mu_l, double a_c, double a_p, double h_c, double h_p, double kernel_precision){
 NumericVector new_litter_vec(ncell);
 NumericVector new_consumer_vec(ncell);
 NumericVector new_predator_vec(ncell);
 NumericVector consumer_mortality(ncell);
 NumericVector consumer_foraging(ncell);
 NumericVector consumer_netdisp(ncell);
 NumericVector consumer_predmortality(ncell);
 NumericVector predator_mortality(ncell);
 NumericVector predator_foraging(ncell);
 NumericVector predator_netdisp(ncell);

 NumericVector Fc(ncell);
 NumericVector Fp(ncell);
 NumericVector Dc(ncell);
 NumericVector Dp(ncell);
 double denominator_Fc=0.0;
 double denominator_Fp=0.0;
 double denominator_Dc=0.0;
 double denominator_Dp=0.0;
 Fc.fill(0);
 Fp.fill(0);
 Dc.fill(0);
 Dp.fill(0);
 for (int i=0;i<ncell;i++){
  Fc[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Fc*sigma_Fc)));
  Fp[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Fp*sigma_Fp)));
  Dc[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Dc*sigma_Dc)));
  Dp[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Dp*sigma_Dp)));
 }
 for (int j=0;j<(2*ncell-1);j++){
  denominator_Fc=denominator_Fc+Fc[(abs(ncell-1-j))];
  denominator_Fp=denominator_Fp+Fp[(abs(ncell-1-j))];
  denominator_Dc=denominator_Dc+Dc[(abs(ncell-1-j))];
  denominator_Dp=denominator_Dp+Dp[(abs(ncell-1-j))];
 }
 for (int i=0;i<ncell;i++){
  Fc[i]=Fc[i]/denominator_Fc;
  Fp[i]=Fp[i]/denominator_Fp;
  Dc[i]=Dc[i]/denominator_Dc;
  Dp[i]=Dp[i]/denominator_Dp;
 }

 int n_kernel_fc=ncell-1;
 int n_kernel_fp=ncell-1;
 int n_kernel_dc=ncell-1;
 int n_kernel_dp=ncell-1;
 if (kernel_precision<1.0){
  int it=1;
  double test=Fc[0];
  while(it<ncell){
   test+=(2*Fc[it]);
   if (test>kernel_precision){
    n_kernel_fc=it;
    it=ncell;
   }
   it++;
  }
  it=1;
  test=Fp[0];
  while(it<ncell){
   test+=(2*Fp[it]);
   if (test>kernel_precision){
    n_kernel_fp=it;
    it=ncell;
   }
   it++;
  }
  it=1;
  test=Dc[0];
  while(it<ncell){
   test+=(2*Dc[it]);
   if (test>kernel_precision){
    n_kernel_dc=it;
    it=ncell;
   }
   it++;
  }
  it=1;
  test=Dp[0];
  while(it<ncell){
   test+=(2*Dp[it]);
   if (test>kernel_precision){
    n_kernel_dp=it;
    it=ncell;
   }
   it++;
  }
 }

 double integral_lc;
 double integral_cl;
 double integral_cp;
 double integral_cdc;
 double integral_pc;
 double integral_pdp;


  for (int i=0;i<ncell;i++){
   new_litter_vec[i]=litter_vec[i]*(1.0-mu_l);
   new_consumer_vec[i]=consumer_vec[i]*(1.0-mu_c*consumer_vec[i]);
   consumer_mortality[i]=mu_c*consumer_vec[i]*consumer_vec[i];
   new_predator_vec[i]=predator_vec[i]*(1.0-mu_p*predator_vec[i]);
   predator_mortality[i]=mu_p*predator_vec[i]*predator_vec[i];
  }

  for (int i=0;i<ncell;i++){
   integral_lc=0.0;
   integral_cl=0.0;
   for (int j=(i-n_kernel_fc);j<(i+n_kernel_fc+1);j++){
    integral_lc-=(Fc[(abs(j-i))]*new_consumer_vec[(reflect(j,ncell))]);
    integral_cl+=(Fc[(abs(j-i))]*new_litter_vec[(reflect(j,ncell))]/(1.0+h_c*a_c*new_litter_vec[(reflect(j,ncell))]));
   }

   integral_cp=0.0;
   integral_pc=0.0;
   for (int j=(i-n_kernel_fp);j<(i+n_kernel_fp+1);j++){
    integral_cp-=(new_predator_vec[(reflect(j,ncell))]*Fp[(abs(j-i))]);
    integral_pc+=(Fp[(abs(j-i))]*new_consumer_vec[(reflect(j,ncell))]/(1.0+h_p*a_p*new_consumer_vec[(reflect(j,ncell))]*(1.0-d_c)));
   }

   integral_cdc=0.0;
   for (int j=(i-n_kernel_dc);j<(i+n_kernel_dc+1);j++){
    integral_cdc+=(Dc[(abs(j-i))]*new_consumer_vec[(reflect(j,ncell))]);
   }

   integral_pdp=0.0;
   for (int j=(i-n_kernel_dp);j<(i+n_kernel_dp+1);j++){
    integral_pdp+=(Dp[(abs(j-i))]*new_predator_vec[(reflect(j,ncell))]);
   }

   consumer_foraging[i]=(epsilon_c*(1.0-d_c)*new_consumer_vec[i]*a_c*integral_cl);
   consumer_netdisp[i]=(d_c*integral_cdc)-d_c*new_consumer_vec[i];
   consumer_predmortality[i]=((1.0-d_p)*integral_cp*a_p*(1.0-d_c)*new_consumer_vec[i]/(1.0+h_p*a_p*(1.0-d_c)*new_consumer_vec[i]));
   predator_foraging[i]=(epsilon_p*(1.0-d_p)*new_predator_vec[i]*a_p*(1.0-d_c)*integral_pc);
   predator_netdisp[i]=(d_p*integral_pdp)-d_p*new_predator_vec[i];

 }


 List res = List::create(consumer_mortality,consumer_foraging,consumer_netdisp,consumer_predmortality,predator_mortality,predator_foraging,predator_netdisp);
 return res;
}

// [[Rcpp::export]]
List compute_bottom_up_bitrophic_cpp(NumericVector litter_vec,NumericVector consumer_vec, int ncell,double deltaX,double sigma_Fc,double sigma_Dc,NumericVector I_litter,double epsilon_c,double d_c,double mu_c,double mu_l, double a_c, double h_c, double kernel_precision){
 NumericVector new_litter_vec(ncell);
 NumericVector new_consumer_vec(ncell);
 NumericVector consumer_mortality(ncell);
 NumericVector consumer_foraging(ncell);
 NumericVector consumer_netdisp(ncell);

 NumericVector Fc(ncell);
 NumericVector Dc(ncell);
 double denominator_Fc=0.0;
 double denominator_Dc=0.0;
 Fc.fill(0);
 Dc.fill(0);
 for (int i=0;i<ncell;i++){
  Fc[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Fc*sigma_Fc)));
  Dc[i]=exp(-(i*i*deltaX*deltaX/(2*sigma_Dc*sigma_Dc)));
 }
 for (int j=0;j<(2*ncell-1);j++){
  denominator_Fc=denominator_Fc+Fc[(abs(ncell-1-j))];
  denominator_Dc=denominator_Dc+Dc[(abs(ncell-1-j))];
 }
 for (int i=0;i<ncell;i++){
  Fc[i]=Fc[i]/denominator_Fc;
  Dc[i]=Dc[i]/denominator_Dc;
 }

 int n_kernel_fc=ncell-1;
 int n_kernel_dc=ncell-1;
 if (kernel_precision<1.0){
  int it=1;
  double test=Fc[0];
  while(it<ncell){
   test+=(2*Fc[it]);
   if (test>kernel_precision){
    n_kernel_fc=it;
    it=ncell;
   }
   it++;
  }
  it=1;
  test=Dc[0];
  while(it<ncell){
   test+=(2*Dc[it]);
   if (test>kernel_precision){
    n_kernel_dc=it;
    it=ncell;
   }
   it++;
  }
 }

 double integral_lc;
 double integral_cl;
 double integral_cdc;


  for (int i=0;i<ncell;i++){
   new_litter_vec[i]=litter_vec[i]*(1.0-mu_l);
   new_consumer_vec[i]=consumer_vec[i]*(1.0-mu_c*consumer_vec[i]);
   consumer_mortality[i]=mu_c*consumer_vec[i]*consumer_vec[i];
  }

  for (int i=0;i<ncell;i++){
   integral_lc=0.0;
   integral_cl=0.0;
   for (int j=(i-n_kernel_fc);j<(i+n_kernel_fc+1);j++){
    integral_lc-=(Fc[(abs(j-i))]*new_consumer_vec[(reflect(j,ncell))]);
    integral_cl+=(Fc[(abs(j-i))]*new_litter_vec[(reflect(j,ncell))]/(1.0+h_c*a_c*new_litter_vec[(reflect(j,ncell))]));
   }

   integral_cdc=0.0;
   for (int j=(i-n_kernel_dc);j<(i+n_kernel_dc+1);j++){
    integral_cdc+=(Dc[(abs(j-i))]*new_consumer_vec[(reflect(j,ncell))]);
   }

   consumer_foraging[i]=(epsilon_c*(1.0-d_c)*new_consumer_vec[i]*a_c*integral_cl);
   consumer_netdisp[i]=(d_c*integral_cdc)-d_c*new_consumer_vec[i];
 }


 List res = List::create(consumer_mortality,consumer_foraging,consumer_netdisp);
 return res;
}

// [[Rcpp::export]]
List spatial_foodweb_dynamics_reflective_boundary(NumericVector litter_vec_ini,NumericMatrix animal_matrix_ini,int nspec, int ncell,double deltaX,NumericVector sigma_F,NumericVector sigma_D,NumericVector I_litter,NumericMatrix epsilon,NumericVector d,NumericVector mu,double mu_l,NumericMatrix a,NumericMatrix h,int nstep,double kernel_precision){
 NumericVector litter_vec=clone(litter_vec_ini);
 NumericMatrix animal_matrix=clone(animal_matrix_ini);

 NumericVector new_litter_vec(ncell);
 new_litter_vec.fill(0.0);

 NumericMatrix new_animal_matrix(nspec,ncell);
 NumericMatrix F(nspec,ncell);
 NumericMatrix D(nspec,ncell);

 for (int j=0;j<nspec;j++){
  for (int i=0;i<ncell;i++){
   F(j,i)=exp(-(i*i*deltaX*deltaX/(2*sigma_F[j]*sigma_F[j])));
   D(j,i)=exp(-(i*i*deltaX*deltaX/(2*sigma_D[j]*sigma_D[j])));
   new_animal_matrix(j,i)=0.0;
  }
 }

 NumericVector denominator_F(nspec);
 NumericVector denominator_D(nspec);
 for (int j=0;j<nspec;j++){
  for (int i=0;i<(2*ncell-1);i++){
   denominator_F[j]=denominator_F[j]+F(j,(abs(ncell-1-i)));
   denominator_D[j]=denominator_D[j]+D(j,(abs(ncell-1-i)));
  }
 }

 for (int j=0;j<nspec;j++){
  for (int i=0;i<ncell;i++){
   F(j,i)=F(j,i)/denominator_F[j];
   D(j,i)=D(j,i)/denominator_D[j];
  }
 }

 NumericVector n_kernel_F(nspec);
 NumericVector n_kernel_D(nspec);
 n_kernel_F.fill(ncell-1);
 n_kernel_D.fill(ncell-1);
 for (int j=0;j<nspec;j++){
  if (kernel_precision<1.0){
   int it=1;
   double test=F(j,0);
   while(it<ncell){
    test+=(2*F(j,it));
    if (test>kernel_precision){
     n_kernel_F[j]=it;
     it=ncell;
    }
    it++;
   }
   it=1;
   test=D(j,0);
   while(it<ncell){
    test+=(2*D(j,it));
    if (test>kernel_precision){
     n_kernel_D[j]=it;
     it=ncell;
    }
    it++;
   }
  }
 }

 NumericVector integral_lc(nspec);
 NumericVector integral_cl(nspec);
 NumericVector integral_cp(nspec);
 NumericVector integral_pc(nspec);
 NumericVector integral_cdc(nspec);


 for (int istep=0;istep<nstep;istep++){
  for (int i=0;i<ncell;i++){
    new_litter_vec[i]=litter_vec[i]*(1.0-mu_l);
    litter_vec[i]=new_litter_vec[i]+I_litter[i];
  }
  for (int j=0;j<nspec;j++){
   for (int i=0;i<ncell;i++){
    new_animal_matrix(j,i)=animal_matrix(j,i)*(1.0-mu[j]*animal_matrix(j,i));
   }
  }

  for (int k=0;k<nspec;k++){
   for (int i=0;i<ncell;i++){
    integral_lc[k]=0.0;
    integral_cl[k]=0.0;
    for (int j=(i-n_kernel_F[k]);j<(i+n_kernel_F[k]+1);j++){
     integral_lc[k]-=(F(k,(abs(j-i)))*new_animal_matrix(k,(reflect(j,ncell))));
     integral_cl[k]+=(F(k,(abs(j-i)))*new_litter_vec[(reflect(j,ncell))]/(1.0+h(k,nspec)*a(k,nspec)*new_litter_vec[(reflect(j,ncell))]));
    }

    integral_pc[k]=0.0;
    for (int k2=0;k2<nspec;k2++){
     for (int j=(i-n_kernel_F[k]);j<(i+n_kernel_F[k]+1);j++){
      integral_pc[k]+=(F(k,(abs(j-i)))*epsilon(k,k2)*a(k,k2)*(1.0-d[k2])*new_animal_matrix(k2,(reflect(j,ncell)))/(1.0+h(k,k2)*a(k,k2)*new_animal_matrix(k2,(reflect(j,ncell)))*(1.0-d[k2])));
     }
    }

    integral_cp[k]=0.0;
    for (int k2=0;k2<nspec;k2++){
     for (int j=(i-n_kernel_F[k2]);j<(i+n_kernel_F[k2]+1);j++){
      integral_cp[k]-=((1.0-d[k2])*new_animal_matrix(k2,(reflect(j,ncell)))*F(k2,(abs(j-i)))*a(k2,k)/(1.0+h(k2,k)*a(k2,k)*(1.0-d[k])*new_animal_matrix(k,i)));
     }
    }

    integral_cdc[k]=0.0;
    for (int j=(i-n_kernel_D[k]);j<(i+n_kernel_D[k]+1);j++){
     integral_cdc[k]+=(D(k,(abs(j-i)))*new_animal_matrix(k,(reflect(j,ncell))));
    }

    litter_vec[i]+=((1.0-d[k])*a(k,nspec)*new_litter_vec[i]*integral_lc[k]/(1.0+h(k,nspec)*a(k,nspec)*new_litter_vec[i]));

    animal_matrix(k,i)=((1.0-d[k])*new_animal_matrix(k,i)+epsilon(k,nspec)*(1.0-d[k])*new_animal_matrix(k,i)*a(k,nspec)*integral_cl[k]+(d[k]*integral_cdc[k]))+((1.0-d[k])*new_animal_matrix(k,i)*integral_pc[k])+(integral_cp[k]*(1.0-d[k])*new_animal_matrix(k,i));
   }
  }
 }

 List res = List::create(litter_vec,animal_matrix);
 return res;
}

