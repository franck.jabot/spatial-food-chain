---
title: "Spatial food chains"
header-includes: \usepackage{amsmath}
output:
  pdf_document: default
  html_document:
    theme: cerulean
---



# 2 - Simulation of spatial tritrophic systems

We implemented this system of integrodifference equations within the R package "spatialfoodchain".

To launch a simulation, after having loaded the package, one must specify a number of input parameters: the spatial resolution $\Delta x$ along the x axis and the width $H$ of each habitat that together determine the number of cells used in the simulation.

```{r}
library(spatialfoodchain)
deltaX=1
H=200
n_cell=2*ceiling(H/deltaX)
```

One must also specifiy all the model parameters detailed above in the mathematical section.

```{r}
## assimilation coefficients
epsilon_c=0.1
epsilon_p=0.7

## mortality (leaching) coefficients
mu_c=0.1
mu_p=0.1
mu_l=0.1

## consumption coefficients of the Holling type 2 functions
a_p=0.1
a_c=1
h_p=1
h_c=0.1

## dispersal coefficients
d_c=0.01
d_p=0.01

## foraging and dispersal kernels' coefficients - we use centered Gaussian kernels
sigma_Fc=5
sigma_Fp=5
sigma_Dc=25
sigma_Dp=25
```

We are specifically interested in the study of spillover effects at the vicinity of the edge between two habitats. For this, we will study a stylized case where litter input rates $I_L$ differ between the two habitats with a sharp transition (e.g., forest-grassland edge):

```{r}
n_center=10
n_left=0.5*(n_cell-n_center)
n_right=n_left
I_mean=0.55
I_diff=0.9
sigma_I=2
I_litter=compute_gaussian_litter(I_mean,I_diff,sigma_I,n_cell,deltaX)
```

```{r echo=FALSE}
x=-((n_cell-1)*0.5):((n_cell-1)*0.5)
plot(x,I_litter,type="l",xlab="x",ylab="",xlim=c(-((n_cell-1)*0.5+10),((n_cell-1)*0.5)),ylim=c(0,1),las=1,cex.axis=0.7)
mtext(expression(I[L]),side=2,las=1,line=3)
text(-((n_cell-1)*0.25),0.05,"Habitat 1")
text(((n_cell-1)*0.25),0.05,"Habitat 2")
```

Finally, one must provide initial conditions for the state variables $L_t(x)$, $C_t(x)$ and $P_t(x)$ to launch the simulation:

```{r}
litter_vec=I_litter
consumer_vec=array(0.8,n_cell)
predator_vec=array(0.01,n_cell)
```

Then, the simulation can be simply launched with the function "simul_spatial_food_chain_gaussian_edge_litter" that will by default simulate 1000 simulation steps, keeping in memory the state variables every 20 steps (these two ancillary parameters can be changed with the arguments "nstep_element" and "n_step").

```{r}
res=simul_spatial_food_chain_gaussian_edge_litter(I_mean,I_diff,sigma_I,consumer_vec,predator_vec,n_cell,deltaX,sigma_Fc,sigma_Fp,sigma_Dc,sigma_Dp,epsilon_c,epsilon_p,d_c,d_p,mu_c,mu_p,mu_l,a_c,a_p,h_c,h_p)
```

The output format is a list of 50 elements. The 50th element encapsulates the final state. Each element is a list of three vectors corresponding to $L_t(x)$, $C_t(x)$ and $P_t(x)$. The final states can thus be easily plotted:

```{r}
par(mfrow=c(1,3))
plot(res[[50]][[1]],type="b",xlab="x",ylab="Abundance",main="Litter")
plot(res[[50]][[2]],type="b",xlab="x",ylab="Abundance",main="Consumers")
plot(res[[50]][[3]],type="b",xlab="x",ylab="Abundance",main="Predators")
```

Thanks to the intermediary outputs, one must check that the burn-in period was sufficient and that a stable final state has been reached, using the function "burnin_check":

```{r}
burnin_check(res)
```

Two internal functions "edge_effects" and "diagnostic_plots" enable the user to assess the spatial extent of edge effects. For this, one must specify one or several sensitivity thresholds. The spatial extent of the edge effects will then be defined as the maximal distance from the edge at which the abundance of a trophic group differs more from its average value far from the edge than this sensitivity thresholds. In other words, lower sensitivity thresholds will deliver larger associated spatial extents.

The output of the function "edge_effects" is a list of three elements for litter, consumers and predators respectively. For each element, the first/second lines correspond to the left/right edge. The first column details the total impact of the edge, defined as the sum of absolute relative deviations of abundances from the reference value. The following columns (here the columns 2-5) correspond to the spatial extent of the edge effect for each sensitivity threshold. The last two columns correspond to the reference value and the cell number at which this value was taken.

```{r}
sensitivity_thresholds=c(0.01,0.025,0.05,0.1)
metrics=edge_effects(res[[50]],sensitivity_thresholds,n_left,n_right,deltaX)
metrics
```

The output of the function "diagnostic_plots" is a series of three plots. The first one displays the abundance curve in the two habitats, the second and third ones zoom on the left and right edges. The magnitude of this zoom can be controlled with the parameter "n_trunc" that specifies how many left/right cells are discarded in the plot. Horizontal lines display the reference values modulated by each sensitivity threshold. Vertical lines display the spatial extent of edge effects for each sensivity threshold.

```{r}
n_trunc=0.25*(n_cell-1)
diagnostic_plots(res[[50]][[1]],n_cell,n_left,n_right,n_trunc,metrics[[1]],sensitivity_thresholds)
diagnostic_plots(res[[50]][[2]],n_cell,n_left,n_right,n_trunc,metrics[[2]],sensitivity_thresholds)
diagnostic_plots(res[[50]][[3]],n_cell,n_left,n_right,n_trunc,metrics[[3]],sensitivity_thresholds)

```

# 3 - Code optimization

## 3.1 Burn-in period

Burn-in periods should be sufficient to reach a final stable state, but it is useless and costly to make them too long. Our default setting seems appropriate for the range of parameters explored in our simulations. Nevertheless, we recommend users to check the adequacy of the burn-in period with the provided function "burnin_check".

## 3.2 Spatial extent

The spatial extent $H$ should be sufficiently large so that state variables within each habitat stabilize far from the edge. It is however useless and costly to consider spatial extents that are far larger than edge effects. We therefore recommend the use of diagnostic plots to visually assess whether state variables have stabilized far from the edge, and to check that the spatial extent of edge effects computed with the function "edge_effects" do not get too close to $H$ in the simulations.

We demonstrate below the computing cost of extending in the simulation, with negligible changes in simulation results:

```{r}
tab_H=c(200,300,400) # alternative H values
res_tab=list()
computing_time=array(0,3)
for (i in 1:3){
 H=tab_H[i]
 n_cell=2*ceiling(H/deltaX)
 n_left=0.5*(n_cell-n_center)
 n_right=n_left
 I_litter=compute_gaussian_litter(I_mean,I_diff,sigma_I,n_cell,deltaX)
 litter_vec=I_litter
 consumer_vec=array(0.8,n_cell)
 predator_vec=array(0.01,n_cell)
 a=Sys.time()
 res_tab[[i]]=simul_spatial_food_chain_gaussian_edge_litter(I_mean,I_diff,sigma_I,consumer_vec,predator_vec,n_cell,deltaX,sigma_Fc,sigma_Fp,sigma_Dc,sigma_Dp,epsilon_c,epsilon_p,d_c,d_p,mu_c,mu_p,mu_l,a_c,a_p,h_c,h_p)
 b=Sys.time()
 computing_time[i]=difftime(b,a,units="sec")
}
computing_time
```

```{r}
#litter at the left boundary
res_tab[[1]][[50]][[1]][1]
res_tab[[2]][[50]][[1]][1]
res_tab[[3]][[50]][[1]][1]
```

```{r}
#litter at the right boundary
res_tab[[1]][[50]][[1]][400]
res_tab[[2]][[50]][[1]][600]
res_tab[[3]][[50]][[1]][800]
```

```{r}
#consumers at the left boundary
res_tab[[1]][[50]][[2]][1]
res_tab[[2]][[50]][[2]][1]
res_tab[[3]][[50]][[2]][1]
```

```{r}
#consumers at the right boundary
res_tab[[1]][[50]][[2]][400]
res_tab[[2]][[50]][[2]][600]
res_tab[[3]][[50]][[2]][800]
```

```{r}
#predators at the left boundary
res_tab[[1]][[50]][[3]][1]
res_tab[[2]][[50]][[3]][1]
res_tab[[3]][[50]][[3]][1]
```

```{r}
#predators at the right boundary
res_tab[[1]][[50]][[3]][400]
res_tab[[2]][[50]][[3]][600]
res_tab[[3]][[50]][[3]][800]
```

## 3.3 Spatial resolution

The spatial resolution $\Delta x$ of the simulation controls the level of spatial details of the simulation. A smaller spatial resolution comes at a cost in terms of computing time, since it will lead to an increased number of cells for a given spatial extent $H$ simulated. Biologically, it makes sense to choose $\Delta x$ so that foraging movements of consumers and predators extend over a small number of cells.

We demonstrate below the computing cost of increasing the spatial resolution, with only small changes in simulation results.

```{r}
tab_deltaX=c(1,0.5,0.2) # alternative deltaX values
res_tab_deltaX=list()
computing_time=array(0,3)
metrics_deltaX=list()
H=200
for (i in 1:3){
 deltaX=tab_deltaX[i]
 n_cell=2*ceiling(H/deltaX)
 n_center=10/deltaX
 n_left=0.5*(n_cell-n_center)
 n_right=n_left
 I_litter=compute_gaussian_litter(I_mean,I_diff,sigma_I,n_cell,deltaX)
 litter_vec=I_litter
 consumer_vec=array(0.8,n_cell)
 predator_vec=array(0.01,n_cell)
 a=Sys.time()
 res_tab_deltaX[[i]]=simul_spatial_food_chain_gaussian_edge_litter(I_mean,I_diff,sigma_I,consumer_vec,predator_vec,n_cell,deltaX,sigma_Fc,sigma_Fp,sigma_Dc,sigma_Dp,epsilon_c,epsilon_p,d_c,d_p,mu_c,mu_p,mu_l,a_c,a_p,h_c,h_p)
 b=Sys.time()
 computing_time[i]=difftime(b,a,units="sec")
 metrics_deltaX[[i]]=edge_effects(res_tab_deltaX[[i]][[50]],sensitivity_thresholds,n_left,n_right,deltaX)
}
computing_time
```

```{r}
#relative errors for litter edge effects
(metrics_deltaX[[1]][[1]][,1]-metrics_deltaX[[3]][[1]][,1])/metrics_deltaX[[3]][[1]][,1]
(metrics_deltaX[[2]][[1]][,1]-metrics_deltaX[[3]][[1]][,1])/metrics_deltaX[[3]][[1]][,1]
```

```{r}
#relative errors for consumer edge effects
(metrics_deltaX[[1]][[2]][,1]-metrics_deltaX[[3]][[2]][,1])/metrics_deltaX[[3]][[2]][,1]
(metrics_deltaX[[2]][[2]][,1]-metrics_deltaX[[3]][[2]][,1])/metrics_deltaX[[3]][[2]][,1]
```

```{r}
#relative errors for predator edge effects
(metrics_deltaX[[1]][[3]][,1]-metrics_deltaX[[3]][[3]][,1])/metrics_deltaX[[3]][[3]][,1]
(metrics_deltaX[[2]][[3]][,1]-metrics_deltaX[[3]][[3]][,1])/metrics_deltaX[[3]][[3]][,1]
```

Further coarsening of the spatial resolution ends with measurable changes in simulation results:

```{r}
tab_deltaX2=c(2,5) # alternative deltaX values
res_tab_deltaX2=list()
computing_time=array(0,2)
metrics_deltaX2=list()
for (i in 1:2){
 deltaX=tab_deltaX2[i]
 n_cell=2*ceiling(H/deltaX)
 n_center=2*ceiling(5/deltaX)
 n_left=0.5*(n_cell-n_center)
 n_right=n_left
 I_litter=compute_gaussian_litter(I_mean,I_diff,sigma_I,n_cell,deltaX)
 litter_vec=I_litter
 consumer_vec=array(0.8,n_cell)
 predator_vec=array(0.01,n_cell)
 a=Sys.time()
 res_tab_deltaX2[[i]]=simul_spatial_food_chain_gaussian_edge_litter(I_mean,I_diff,sigma_I,consumer_vec,predator_vec,n_cell,deltaX,sigma_Fc,sigma_Fp,sigma_Dc,sigma_Dp,epsilon_c,epsilon_p,d_c,d_p,mu_c,mu_p,mu_l,a_c,a_p,h_c,h_p)
 b=Sys.time()
 computing_time[i]=difftime(b,a,units="sec")
 metrics_deltaX2[[i]]=edge_effects(res_tab_deltaX2[[i]][[50]],sensitivity_thresholds,n_left,n_right,deltaX)
}
computing_time
```

```{r}
#relative errors for litter edge effects
(metrics_deltaX2[[1]][[1]][,1]-metrics_deltaX[[3]][[1]][,1])/metrics_deltaX[[3]][[1]][,1]
(metrics_deltaX2[[2]][[1]][,1]-metrics_deltaX[[3]][[1]][,1])/metrics_deltaX[[3]][[1]][,1]
```

```{r}
#relative errors for consumer edge effects
(metrics_deltaX2[[1]][[2]][,1]-metrics_deltaX[[3]][[2]][,1])/metrics_deltaX[[3]][[2]][,1]
(metrics_deltaX2[[2]][[2]][,1]-metrics_deltaX[[3]][[2]][,1])/metrics_deltaX[[3]][[2]][,1]
```

```{r}
#relative errors for predator edge effects
(metrics_deltaX2[[1]][[3]][,1]-metrics_deltaX[[3]][[3]][,1])/metrics_deltaX[[3]][[3]][,1]
(metrics_deltaX2[[2]][[3]][,1]-metrics_deltaX[[3]][[3]][,1])/metrics_deltaX[[3]][[3]][,1]
```

## 3.4 Kernel approximations

To speed up the code, we assessed the impacts of truncating the tails of the foraging and dispersal kernels. This can be done with the parameter "kernel_precision" that is by default fixed to 1 (i.e. no kernel approximation), but can be decreased to a value smaller than 1. In this case, the left and right tails of the kernels are set to zero so that the integral value of the truncated kernel equals the value of "kernel_precision". This use of truncated kernel considerably speeds up the simulation. Using a value of 0.9999 for "kernel_precision" leads to negligible changes in simulation results with a ten fold decrease in computing cost. This value will be used in the following.

```{r}
deltaX=1
H=200
n_cell=2*ceiling(H/deltaX)
n_center=10
n_left=0.5*(n_cell-n_center)
n_right=n_left
I_litter=compute_gaussian_litter(I_mean,I_diff,sigma_I,n_cell,deltaX)
tab_kernel=c(0.99,0.995,0.999,0.9995,0.9999,1) # alternative values for kernel_precision
res_tab_kernel=list()
computing_time=array(0,6)
metrics_kernel=list()
for (i in 1:6){
 litter_vec=I_litter
 consumer_vec=array(0.8,n_cell)
 predator_vec=array(0.01,n_cell)
 a=Sys.time()
 res_tab_kernel[[i]]=simul_spatial_food_chain_gaussian_edge_litter(I_mean,I_diff,sigma_I,consumer_vec,predator_vec,n_cell,deltaX,sigma_Fc,sigma_Fp,sigma_Dc,sigma_Dp,epsilon_c,epsilon_p,d_c,d_p,mu_c,mu_p,mu_l,a_c,a_p,h_c,h_p,kernel_precision=tab_kernel[i])
 b=Sys.time()
 computing_time[i]=difftime(b,a,units="sec")
 metrics_kernel[[i]]=edge_effects(res_tab_kernel[[i]][[50]],sensitivity_thresholds,n_left,n_right,deltaX)
}
computing_time
```

```{r}
#relative errors for litter edge effects
for (i in 1:5){
 print((metrics_kernel[[i]][[1]][,1]-metrics_kernel[[6]][[1]][,1])/metrics_kernel[[6]][[1]][,1])
}
```

```{r}
#relative errors for consumer edge effects
for (i in 1:5){
 print((metrics_kernel[[i]][[2]][,1]-metrics_kernel[[6]][[2]][,1])/metrics_kernel[[6]][[2]][,1])
}
```

```{r}
#relative errors for predator edge effects
for (i in 1:5){
 print((metrics_kernel[[i]][[3]][,1]-metrics_kernel[[6]][[3]][,1])/metrics_kernel[[6]][[3]][,1])
}
```

