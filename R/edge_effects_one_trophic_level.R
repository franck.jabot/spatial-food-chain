edge_effects_one_trophic_level<-function(vec,tabx,n_left,n_right,deltaX){
 metrics=matrix(0,2,(4+length(tabx)))
 n_cell=length(vec)
 ref=vec[1]
 refr=vec[n_cell]

metrics[1,1]=sum(abs(vec[1:n_left]-ref)/ref)*deltaX
 metrics[2,1]=sum(abs(vec[(n_cell-n_right+1):n_cell]-refr)/refr)*deltaX
 for (i in 1:length(tabx)){
  test=(vec[1:n_left]>ref*(1+tabx[i]))+(vec[1:n_left]<ref*(1-tabx[i]))

  if (sum(test)>0){
   metrics[1,(i+1)]=(1+length(test)-min((1:length(test))[test>0]))*deltaX
  }
  test=(vec[(n_cell-n_right+1):n_cell]>refr*(1+tabx[i]))+(vec[(n_cell-n_right+1):n_cell]<refr*(1-tabx[i]))

  if (sum(test)>0){
   metrics[2,(i+1)]=max((1:length(test))[test>0])*deltaX
  }
 }
 metrics[1,(2+length(tabx))]=ref
 metrics[1,(3+length(tabx))]=1
 metrics[1,(4+length(tabx))]=sum(abs(vec[1:n_left]-ref))*deltaX
 metrics[2,(2+length(tabx))]=refr
 metrics[2,(3+length(tabx))]=n_cell
 metrics[2,(4+length(tabx))]=sum(abs(vec[(n_cell-n_right+1):n_cell]-refr))*deltaX

metrics
}